# -*- coding: utf-8 -*-
{
    'name': 'Module Siswa',
    'summary': 'Module Siswa',
    'description': """
        Module Siswa
    """,
    'version': '1.2.35',
    'category': 'Others',
    'author': 'Sefto',
    'support': 'info@sefto.com',
    'website': 'https://www.sefto.com',
    'depends': ['base',],
    'data': [
        'views/siswa_view.xml',
        'security/ir.model.access.csv',
    ],
    'installable': True,
    'application': True,
}