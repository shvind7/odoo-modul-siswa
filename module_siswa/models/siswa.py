# -*- coding: utf-8 -*-

from odoo import api, fields, models, _


class Siswa(models.Model):
    _description = "Siswa"
    _name = 'model.siswa'

    name = fields.Char('Siswa')
    date_of_birth = fields.Date('Tangal Lahir')
    kelas = fields.Many2one('model.kelas', string='Kelas')
    guru = fields.Many2one('model.guru', string='Guru')
    mata_pelajaran = fields.Many2one('model.mapel', string='Mata Pelajaran', related='guru.mapel')


class Guru(models.Model):
    _description = "Guru"
    _name = 'model.guru'

    name = fields.Char('Guru')
    mapel = fields.Many2one('model.mapel', string='Mata Pelajaran')


class MataPelajaran(models.Model):
    _description = "Mata Pelajaran"
    _name = 'model.mapel'

    name = fields.Char('Mata Pelajaran')


class Kelas(models.Model):
    _description = "Kelas"
    _name = 'model.kelas'

    name = fields.Char('Kelas')